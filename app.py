from aiohttp import web
from characters.routes import setup_routes
from characters.settings import config


app = web.Application()
app['config'] = config
try:
    public_key = config['marvel']['api']['keys']['private']
    private_key = config['marvel']['api']['keys']['public']
except KeyError:
    print('Public or private keys are not defined')
setup_routes(app)

web.run_app(app)