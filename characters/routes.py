from characters.views import character_list

def setup_routes(app):
    app.router.add_get('/characters/{name}', character_list)
