import asyncio
import time
import math
import sys
import urllib
import hashlib
import collections
import logging
import traceback

import aiohttp

from aiohttp import ContentTypeError
from characters.settings import config


logger = logging.getLogger(__name__)


MARVEL_API_PRIVATE_KEY=config['marvel']['api']['keys']['private']
MARVEL_API_PUBLIC_KEY=config['marvel']['api']['keys']['public']
MARVEL_API_PREFIX=config['marvel']['api']['prefix']
MARVEL_API_LIMIT=min(100, int(config['marvel']['api']['limit']))
MARVEL_API_TIMEOUT=config['marvel']['api']['timeout']


def get_timestamp():
    return int(time.time())


def make_marvel_api_url(api_suffix, offset=0, limit=MARVEL_API_LIMIT, params=None):

    ts = str(get_timestamp())
    private = MARVEL_API_PRIVATE_KEY
    public = MARVEL_API_PUBLIC_KEY
    h = hashlib.md5(bytes(ts + private + public, encoding='utf-8'))
    hash = h.hexdigest()

    params = params if params is not None else {}
    params = collections.OrderedDict(params)
    params.update(
        offset=offset,
        limit=limit,
        ts=ts,
        apikey=public,
        hash=hash
    )
    params_string = '&'.join(map('{0}={1}'.format, params.keys(), params.values()))
    url = '{api_prefix}/{api_suffix}?{params_string}'.format(
        api_prefix=MARVEL_API_PREFIX,
        api_suffix=api_suffix,
        params_string=params_string,
    )

    return url


def make_characters_url(character_name, **kw):
    return make_marvel_api_url('characters', params=dict(nameStartsWith=character_name), **kw)


def make_creators_url(creator_id, **kw):
    return make_marvel_api_url('creators/{creator_id}'.format(creator_id=creator_id))


def make_related_entity_url(character_id, entity_name):
    api_suffix = 'characters/{character_id}/{entiy_name}'.format(
        character_id=character_id,
        entiy_name=entity_name,
    )

    return make_marvel_api_url(api_suffix, params=dict(orderBy='-modified'))


def make_comics_url(character_id):
    return make_related_entity_url(character_id, 'comics')


def make_events_url(character_id):
    return make_related_entity_url(character_id, 'events')


async def fetch_data(url, reraise=False):
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url, timeout=MARVEL_API_TIMEOUT) as response:
                data = await response.json()
                data['fetched_url'] = url
                return data

    except Exception as e:
        exc_type, exc_value, _ = sys.exc_info()
        logger.error('%s: %s\n%s', exc_type, exc_value, traceback.format_exc())

        if reraise:
            raise e

        return None


async def fetch_by(url):
    try:
        return await fetch_data(url)
    except ContentTypeError as e:
        print(f'ContentTypeError{str(e)}')
        return None


async def fetch_entity_instances(url):
    data = await fetch_by(url)
    return data['data']['results']


async def fetch_entities_data(urls, default=None):
    timeout = MARVEL_API_TIMEOUT
    done, _ = await asyncio.wait([asyncio.ensure_future(fetch_data(url)) for url in urls], timeout=timeout)
    entities_map = {}
    for future in done:
        result = future.result()
        if result:
            entities_map[result.get('fetched_url')] = result['data']['results']
    return [entities_map.get(url, default) for url in urls]


def extract_fields(entity, names):
    return {name: entity.get(name, 'no'+name) for name in names}


async def fetch_character(character_id, data):
    data = data if data is not None else {}
    urls = [make_comics_url(character_id), make_events_url(character_id)]
    comics, events = await fetch_entities_data(urls)

    character = {}
    character['id'] = data.get('id', 'noid')
    character['name'] = data.get('name', 'noname')
    character['description'] = data.get('description', 'nodescription')
    if comics:
        character['comics'] = [extract_fields(el, ['id', 'title', 'description']) for el in comics]
    if events:
        character['events'] = [extract_fields(el, ['id', 'title', 'description']) for el in events]

    creator_ids = set()
    if comics:
        for com in comics:
            creator_ids |= {item.get('resourceURI').rsplit('/')[-1] for item in com.get('creators', {}).get('items', [])}
    creator_ids = list(creator_ids)
    creator_fields = ["id", "firstName", "middleName", "lastName", "suffix", "fullName"]
    creators = sum(await fetch_entities_data([make_creators_url(creator_id) for creator_id in creator_ids], default=[]), [])
    if creators:
        character['creators'] = [extract_fields(el, creator_fields) for el in creators]

    return character


async def fetch_characters(character_name):
    single_data = await fetch_data(make_characters_url(character_name, limit=1))
    total = single_data['data']['total']

    if total == 0:
        return []

    characters = []
    for page in range(math.ceil(total/MARVEL_API_LIMIT)):
        offset = page * MARVEL_API_LIMIT
        limit = MARVEL_API_LIMIT
        limited_data = await fetch_data(make_characters_url(character_name, offset=offset, limit=limit))
        for character_data in limited_data['data']['results']:
            character = await fetch_character(character_data['id'], data=character_data)
            characters.append(character)

    return characters