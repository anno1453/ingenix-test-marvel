import sys
import logging
import traceback

import aiohttp

from characters.core import fetch_characters


logger = logging.getLogger(__name__)


async def character_list(request):
    character_name = request.match_info['name']

    try:
        characters = await fetch_characters(character_name)

    except:
        exc_type, exc_value, _ = sys.exc_info()
        logger.error('%s: %s\n%s', exc_type, exc_value, traceback.format_exc())
        return aiohttp.web.Response(text='{}: {}'.format(exc_type.__name__, str(exc_value)), status=400)

    if len(characters) == 0:
        return aiohttp.web.Response(text='Character with name = {} not found'.format(character_name), status=404)

    data  = {
        'results': characters,
        'count': len(characters)
    }

    return aiohttp.web.json_response(data)