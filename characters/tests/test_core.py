import reprlib

import characters.core
from characters.core import make_related_entity_url, make_comics_url, make_events_url, MARVEL_API_PREFIX


def test_make_url(aiohttp_client, loop):
    returned = characters.core.make_marvel_api_url('characters/1009610/comics', params=dict(orderBy='-modified'))

    assert returned.startswith(MARVEL_API_PREFIX)
    assert 'characters/1009610/comics' in returned
    assert 'orderBy=-modified' in returned


def test_make_character_url(aiohttp_client, loop):
    returned = characters.core.make_characters_url('super')

    assert returned.startswith(MARVEL_API_PREFIX)
    assert 'characters?nameStartsWith=' in returned


def test_make_related_entity_url(aiohttp_client, loop):
    character_id = 1009610
    returned = make_related_entity_url(character_id, 'comics')

    assert returned.startswith(MARVEL_API_PREFIX)
    assert '1009610/comics' in returned


def test_make_entities_url(aiohttp_client, loop):
    character_id = 1009610
    urls = [make_comics_url(character_id), make_events_url(character_id)]

    assert all(url.startswith(MARVEL_API_PREFIX) for url in urls)
    assert all(str(character_id) in url for url in urls)
