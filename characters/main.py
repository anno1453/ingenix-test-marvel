# Данный файл используется для запуска сервера на стадии разработки

from aiohttp import web
from characters.routes import setup_routes
from characters.settings import config


app = web.Application()
app['config'] = config
setup_routes(app)
